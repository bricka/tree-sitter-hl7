module.exports = grammar({
    name: 'hl7',

    rules: {
        message: $ => seq(
            $.segment,
            repeat(seq(
                '\n',
                $.segment
            )),
            optional('\n'),
        ),

        segment: $ => seq(
            $.segment_id,
            repeat(seq(
                $.pipe,
                optional($.field),
            )),
        ),

        segment_id: $ => /[A-Z0-9]{3}/,

        field: $ => repeat1(choice(
            /[^\|\^]/,
            $.hat,
        )),

        component: $ => /[^|^]+/,

        pipe: $ => '|',
        hat: $ => '^',
    }
});
