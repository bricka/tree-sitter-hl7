This is a parser for [HL7v2 messages](https://en.wikipedia.org/wiki/Health_Level_7#Version_2_messaging) built on top of the easy-to-use [tree-sitter](https://tree-sitter.github.io/tree-sitter/) parser generator library.

# How to Use

You will need to have the `tree-sitter-cli` package installed and configured:

```shell
npm i -g tree-sitter-cli
tree-sitter init-config
```

You can then generate the parser:
```shell
tree-sitter generate
```

With the parser, you can then parse files that end in `.hl7`, or see how they would be highlighted:

```shell
tree-sitter parse ~/file.hl7
tree-sitter highlight ~/file.hl7
```
